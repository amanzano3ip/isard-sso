# SAML Authentication

The authentication it is done with SAML plugins in the apps agains a central Keycloak identity provider (IdP) server.  As the integration of client SAML with IdP is quite tedious this document describes how to integrate some of this apps into your keycloak server.

## Keycloak

The identity provider administration interface can be reached at https://sso.<yourdomain>. There you can add client apps, roles, groups, users, mappers, etc... Please read documentation at: https://www.keycloak.org/documentation

## Applications

In this document we will cover **Moodle**, **Nextcloud** and **Wordpress** SAML configuration plugins.

### Moodle

Install SAML plugin and follow this steps in **Moodle**:

1. Activate SAML2 plugin at the **Extensions** -> **Authentication**  in Moodle. You should click on the eye. Then enter de *configuration* link.
2. Click on the **Regenerate SP certificate** button. Optionally set up your desired certificate data and accept. You will need to get back to SAML2 configuration plugin afterwards. The direct link page is: https://moodle.<yourdomain>/auth/saml2/regenerate.php
3. Click on the **Lock down** certificate button and accept. This will avoid SAML2 plugin to regenerate the certificate each time we restart Moodle (why has this annoying behaviour?)
4. Download **SAML2 Service Provider** xml and save it in a file (better right click and save to file). The direct link page is: https://moodle.<yourdomain>/auth/saml2/sp/metadata.php

Now go to your *keycloak* admin (https://sso.<yourdomain>.) and:

1. At **Clients** menú go to **create** new client and import the moodle **SAML2 Service Provider** xml and accept. The direct link is: https://sso.<yourdomain>/auth/admin/master/console/#/create/client/master
2. Now go to **Mappers** tab in this client and add this builtins:
   1. *email*
   2. *givenName*
   3. *surname*
3. Now create **Custom Mapper** for username:
   1. Name: `username`
      Mapper Type: *User Property*
      Property: `username`
      Friendly Name: `username`
      SAML Attribute Name: `username`
      SAML Attribute NameFormat: *Basic*
4. Now *copy* the keycloak **SAML IdP xml data
5. Copy keycloak **SAML 2.0 Identity Provider Metadata** xml from **Realm Settings** and paste it into moodle SAML2 plugin **IdP metadata xml OR public xml URL** textbox. Note that you should copy it from a text editor, not the content from the browser view! The content should be one-liner!!! No break lines even it is an xml file.

Now go back to Moodle (if not already there as you just copied the IdP metadata into de SAML2 plugin) and set up this options in the SAML2 plugin:

- **Dual login**: No. But you should be aware that setting to *No* will automatically redirect from now on the logins to Keycloak SSO login page and you won't be able to access your moodle instance as admin if you don't use the alternate url **/login/index.php?saml=off**. This is what you want when you have checked that the SAML2 plugin is working as expected.
- **Mapping idP**: username
- **Auto create users**: Yes
- **Role Mapping**: Configure the same roles you created in keycloak for users. For example:
  - Site administrators: admin
  - Course creator: coursecreator
  - Manager: manager
- **Data mapping**:  With at least this fields moodle will skip user profile fill the first time he logs in as this three are the only required. You can set up more mappers in keycloak for this moodle SP and map it to the rest of the fields.
   1. *email*
   2. *givenName*
   3. *surname*
   
If everything went ok you should now be redirected to Keycloak SSO login page and you can log in into your moodle with the users you already created in Keycloak and with the role assigned to them.

### NEXTCLOUD

TODO: Does not map email nor friendlyname (display name). Also not tested to add quota field in keycloak and map it to nextcloud.

(all credits to this hell set up goes to: https://janikvonrotz.ch/2020/04/21/configure-saml-authentication-for-nextcloud-with-keycloack/, RESPECT)

BEAWARE: of the good programmers, but very bad designers (I empathyze with them but...) The SAML plugin in nextcloud has some greyed out texts that are links! Damn, who did that! You don't realize that are links that open a lot of options that need to be filled in!

1. Copy the Keycloak realm RSA certificate from **Realm Settings** menu, in the **Keys** tab by clicking in the **Certificate** button of **RSA** (not the public key). It will show it in a modal form, just select it, copy and save it into file for later use.
2. Generate Nextcloud SP keys. Sorry, this step is needed. So you should generate your own ones. If you don't know how to install this just enter the nextcloud container (docker exec -ti isard-apps-nextcloud-app /bin/sh) and run the command there and copy the contents elsewhere with the *private.key* and *public.cert* names.
   1. **openssl req  -nodes -new -x509  -keyout private.key -out public.cert**
3. Install SAML plugin. Select **Integrated configuration** at first config page.
4. Configure at: https://nextcloud.<yourdomain>/settings/admin/saml or going to the **Settings** options in user menú.
   1. **General**
      1. Input box: **Attribute to map the UID to**: username
      2. Input box: **Optional display name**: *anything you want as this won't be shown when we activate the direct redirect to keycloak SSO login.
   2. **Service Provider Data**: WARNING: Copy them with BEGIN/END tags! WARNING nº2: You should click on the greyed out link, that doesn't seem a link, to the end of the line.
      1. **x509**: public.key (generated before)
      2. **Private key**: private.key (generated before)
   3. **Identity Provider Data** 
      1. **Identifier of the IdP**: https://sso.<yourdomain>/auth/realms/master
      2. **URL target of the IdP**: https://sso.<yourdomain>/auth/realms/master/protocol/saml
      3. **URL Location of the IdP SLO request**: https://sso.<domain>/auth/realms/poc/protocol/saml
      4. **Public X.509 certificate**: (The *RSA Certificate* from keycloak at the very first step number 1). 
   4. **Attribute mapping**
      1. **email**: email
      2. **user groups**: Role
   5. **Security Settings** (check only this options)
      1. **Signatures and encryption offered**
         1. <samlp: AuthnRequest>
         2. <samlp: logoutRequest>
         3. <samlp: logoutResponse>
      2. **Signatures and encryption required**
         1. <samlp: Response>
         2. <samlp: Assertion> [Metadata of the SP will offer this info]
   6. Click and save the xml metadata from the bottom page button **Download XML metadata**.

If you reached this point you are almost done with Nextcloud SAML configuration if the *annoying* live update of this plugin page shows at the bottom the **Download XML metadata** with no errors. Now let's go back to **Keycloak admin console** and finish configuration.

1. At **Clients** menú go to **create** new client and import the nextcloud **SAML2 Service Provider** xml that you just downloaded and accept. The direct link is: https://sso.<yourdomain>/auth/admin/master/console/#/create/client/master.
   1. My guru that I referenced at the beginning of this documentation says that you should set the **Client SAML Endpoint** to https://sso.<yourdomain>/auth/realms/master prior to accepting the uploaded xml data. I tested that this is not really needed.
2. Now go to **Mappers** tab in this client and create **Custom Mapper** fields: NOTE:  ONLY USERNAME and ROLES WORKING. Nextcloud doesn't get email
   1. Name: `username`
         Mapper Type: *User Property*
         Property: `username`
         Friendly Name: `username`
         SAML Attribute Name: `username`
         SAML Attribute NameFormat: *Basic*
   2. Add builtins:Name: `email`
         Mapper Type: *User Property*
         Property: `email`
         Friendly Name: `email`
         SAML Attribute Name: `email`
         SAML Attribute NameFormat: *Basic*
   3. Name: `roles`
         Mapper Type: *Role List*
         Role attribute name: `Roles`
         Friendly Name: `Roles`
         SAML Attribute NameFormat: *Basic*
         Single Role Attribute: *On*
3. Then the main role seem to be from a single role attribute that should be set up in...
   1. **Client scopes** menu
      1. role_list
         1. Mappers tab
            1. role list
               1. Single Role Attribute, that should be checked.

Now you should be able to test your Keycloak users/roles against nextcloud. If you need to access as admin into nextcloud again you should use this end url for your nextcloud domain: **/login?direct=1**

#### Debug SAML plugin

Trust me, this is important to be here as many settings/options/checkboxes can be missconfigured if you were not really awaken today.

Edit in the outside mount volume (/opt/digitaldemocratic/db/src/nextcloud/config/config.php) and restart nextcloud container (docker restart isard-apps-nextcloud-app):
```
<?php
$CONFIG = array (
  'debug' => true,
...
```
### WORDPRESS
If you already set up Moodle and Nextcloud SAML plugins you are already the master of the universe and I will go faster at describing this one as you want to finish this.

NOTE: Client Id in Keycloak has to be exactly **php-saml**. It could be modified and set up at wordpress saml plugin (but better don't do experiments)

1. Install **OneLogin SAML plugin**
2. **STATUS**
   1. Enable
3. **IDENTITY PROVIDER SETTINGS**
   1. IdP ENTITY ID: Anything you want as won't be shown because we will redirect all logins to Keycloak SSO.
   1. **SSO Service Url**: https://sso.digitaldemocratic.net/auth/realms/master/protocol/saml
   2. **SLO Service Url**: https://sso.digitaldemocratic.net/auth/realms/master/protocol/saml
   3. **X.509 Certificate**: Copy the Certificate (not the Public key) from the keycloak realm (https://sso.digitaldemocratic.net/auth/admin/master/console/#/realms/master/keys) without the begin/end lines in the cert.
4. **OPTIONS**
   1. Create user if not exists
   2. Update user data
   3. Force SAML login (To access as admin look for the url at the end of this part)
   4. Single Log Out
   5. Match Wordpress account by: username ???
5. **ATTRIBUTE MAPPING**
   1. Username: username
   2. Email: email
   4. First Name: givenName
   5. Last Name: sn
   6. Role: Role
6. **ROLE MAPPING**
   1. Administrator: admins
   2. Editor: managers
   3. Author: coursecreators
   ...
   4. Multiple role values...: true
7. **CUSTOMIZE ACTIONS AND LINKS**
   1. Stay in WordPress after SLO
8. **ADVANCED SETTINGS**
   1. Sign AuthnRequest
   2. Sign LogoutRequest
   3. Sign LogoutResponse
   4. Service Provider X.509 Certificate & Service Provider Private Key: Generate both and paste it without the begin/end lines:
         openssl req  -nodes -new -x509  -keyout private.key -out public.cert

9. Download **Service Provider metadata** from top and add it to keycloak clients menu
10. Keycloak client mappers:
          1. Name: `username`
                Mapper Type: *User Property*
                Property: `username`
                Friendly Name: `username`
                SAML Attribute Name: `username`
                SAML Attribute NameFormat: *Basic*
          2. Add builtins:Name: `email`
                Mapper Type: *User Property*
                Property: `email`
                Friendly Name: `email`
                SAML Attribute Name: `email`
                SAML Attribute NameFormat: *Basic*
          3. Name: `roles`
                Mapper Type: *Role List*
                Role attribute name: `Roles`
                Friendly Name: `Roles`
                SAML Attribute NameFormat: *Basic*
                Single Role Attribute: *On*

To access as an admin again you should use the url: https://wp.<domain>/wp-login.php?normal

import json, sys
from pprint import pprint

from keycloak import KeycloakAdmin

def keycloak_connect():
    keycloak_admin = KeycloakAdmin(server_url="http://isard-sso-keycloak:8080/auth/",
                                username='admin',
                                password='keycloakkeycloak',
                                realm_name="master",
                                user_realm_name="only_if_other_realm_than_master",
                                client_secret_key="client-secret",
                                verify=True)

def keycloak_dumps():
    print('Dumping keycloak config...')

def Keycloak_imports():
    with open('saml_client.json') as json_file:
        data = json.load(json_file)

if __name__ == "__main__":
    keycloak_connect()
    if sys.argv[1]=='dump':
        keycloak_dumps()

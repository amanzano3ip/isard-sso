#docker exec -t isard-apps-postgresql pg_dumpall -c -U admin > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql
docker exec -t isard-apps-postgresql pg_dumpall -c -U admin | gzip > ./dump_$(date +"%Y-%m-%d_%H_%M_%S").gz
gunzip < $1 | docker exec -i isard-apps-postgresql psql -U admin -d $2

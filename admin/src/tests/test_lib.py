from keycloak import KeycloakOpenID

# Configure client
keycloak_openid = KeycloakOpenID(server_url="http://isard-sso-keycloak:8080/auth/",
                    client_id="admin-cli",
                    realm_name="master",
                    client_secret_key="secret")

# Get WellKnow
config_well_know = keycloak_openid.well_know()

# Get Token
token = keycloak_openid.token("admin", "keycloakkeycloak")
#token = keycloak_openid.token("user", "password", totp="012345")
print(token)

from keycloak import KeycloakAdmin

keycloak_admin = KeycloakAdmin(server_url="http://isard-sso-keycloak:8080/auth/",
                               username='admin',
                               password='keycloakkeycloak',
                               realm_name="master",
                               verify=True)
        
# Add user                       
new_user = keycloak_admin.create_user({"email": "example@example.com",
                    "username": "example@example.com",
                    "enabled": True,
                    "firstName": "Example",
                    "lastName": "Example"})
print(new_user)

user_id_keycloak = keycloak_admin.get_user_id("admin")
print(user_id_keycloak)
import os,time,requests,json,getpass,pprint
import traceback

from keycloak_client_exc import *

class ApiClient():
    def __init__(self,realm='master'):
        ##server=os.environ['KEYCLOAK_HOST']
        server='isard-sso-keycloak'
        self.base_url="http://"+server+":8080/auth/realms/"+realm
        self.headers={"Content-Type": "application/x-www-form-urlencoded"}
        self.payload={'username':'admin',
                        'password':'keycloakkeycloak',
                        'grant_type':'password',
                        'client_id':'admin-cli'}
        self.token=self.get_token()
        self.admin_url="http://"+server+":8080/auth/admin/realms/"+realm
                                                #  /admin/realms/${KEYCLOAK_REALM}/users/${$USER_ID}" 
        self.admin_headers={"Accept": "application/json",
                            "Authorization": "Bearer "+self.token}

    def get_token(self):
        path="/protocol/openid-connect/token"
        resp = requests.post(self.base_url+path, data=self.payload, headers=self.headers)
        if resp.status_code == 200: return json.loads(resp.text)['access_token']
        print("       URL: "+self.base_url+path)    
        print("STATUS CODE: "+str(resp.status_code))
        print("   RESPONSE: "+resp.text)
        exit(1)

    def get(self,path,status_code=200,data={},params={}):
        resp = requests.get(self.admin_url+path, data=data, params=params, headers=self.admin_headers)
        if resp.status_code == status_code: return json.loads(resp.text)
        print("       URL: "+self.admin_url+path)
        print("STATUS CODE: "+str(resp.status_code))
        print("   RESPONSE: "+resp.text)
        raise

    def post(self,path,status_code=200,data={},params={},json={}):
        resp = requests.post(self.admin_url+path, data=data, params=params, json=json, headers=self.admin_headers)
        #if resp.status_code == status_code: return True
        print("       URL: "+self.admin_url+path)
        print("STATUS CODE: "+str(resp.status_code))
        print("   RESPONSE: "+resp.text)
        if resp.status_code == 409: raise keycloakUsernameEmailExists
        raise keycloakError

class KeycloakClient():
    def __init__(self,realm='master'):
        ## REFERENCE: https://www.keycloak.org/docs-api/13.0/rest-api/index.html
        self.api=ApiClient()

    def get_users(self,username=False,exact=True):
        path='/users'
        if not username: return self.api.get(path)
        return self.api.get(path,params={"username":username,'exact':exact})

    def add_user(self,username,first,last,email,password):
        user={"firstName":first,
                "lastName":last, 
                "email":last, 
                "enabled":"true", 
                "username":username,
                "credentials":[{"type":"password",
                                "value":password,
                                "temporary":False}]}
        try:
            self.api.post('/users',status_code=201,json=user)
            return True
        except keycloakExists:
            print('Username or email already exists')
        except:
            traceback.format_exc()
        return False

    def get_groups(self,name=False):
        path='/groups'
        if not name: return self.api.get(path)
        return self.api.get(path,params={"name":name})

    def add_group(self,name,subgroups=False):
        group={"name":name}
        try:
            self.api.post('/groups',status_code=201,json=group)
            return True
        except keycloakExists:
            print('Group name already exists')
        except:
            traceback.format_exc()
        return False

kapi=KeycloakClient()
# print('GET USERS')
# pprint.pprint(kapi.get_users())
# print('GET ADMIN USER')
# pprint.pprint(kapi.get_users(username='admin'))
# print('ADD USER')
# print(kapi.add_user('pepito','Pepito','Grillo','info@info.com','añlsdkjf'))
# print('GET GROUPS')
# pprint.pprint(kapi.get_groups())
print('ADD GROUP')
pprint.pprint(kapi.add_group('pepito'))
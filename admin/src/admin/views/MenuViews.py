#!flask/bin/python
# coding=utf-8
from admin import app
import logging as log
import traceback

from uuid import uuid4
import time,json
import sys,os
from flask import render_template, Response, request, redirect, url_for, jsonify

from pprint import pprint

@app.route('/isard-sso-admin/users', methods=['GET'])
@app.route('/isard-sso-admin/users/<provider>', methods=['POST', 'PUT', 'GET'])
# @login_required
def users(provider=False):
    if request.method == 'POST':
        if provider == 'moodle':
            return json.dumps(app.admin.sync_to_moodle()), 200, {'Content-Type': 'application/json'}
        if provider == 'nextcloud':
            return json.dumps(app.admin.sync_to_nextcloud()), 200, {'Content-Type': 'application/json'}
    return render_template('pages/users.html', title="Users", nav="Users")

@app.route('/isard-sso-admin/users_list')
# @login_required
def users_list():
    return json.dumps(app.admin.get_mix_users()), 200, {'Content-Type': 'application/json'}


@app.route('/isard-sso-admin/roles')
# @login_required
def roles():
    return render_template('pages/roles.html', title="Roles", nav="Roles")

@app.route('/isard-sso-admin/roles_list')
# @login_required
def roles_list():
    return json.dumps(app.admin.get_roles()), 200, {'Content-Type': 'application/json'}


@app.route('/isard-sso-admin/groups')
# @login_required
def groups():
    return render_template('pages/groups.html', title="Groups", nav="Groups")

@app.route('/isard-sso-admin/groups_list')
# @login_required
def groups_list():
    return json.dumps(app.admin.get_mix_groups()), 200, {'Content-Type': 'application/json'}


@app.route('/isard-sso-admin/external', methods=['POST', 'PUT', 'GET'])
# @login_required
def external():
    if request.method == 'POST':
        return json.dumps(app.admin.upload_json(request.get_json(force=True))), 200, {'Content-Type': 'application/json'}
    if request.method == 'PUT':
        return json.dumps(app.admin.sync_external()), 200, {'Content-Type': 'application/json'}
    return render_template('pages/external.html', title="External", nav="External")

@app.route('/isard-sso-admin/external_users_list')
# @login_required
def external_users_list():
    return json.dumps(app.admin.get_external_users()), 200, {'Content-Type': 'application/json'}

@app.route('/isard-sso-admin/external_groups_list')
# @login_required
def external_groups_list():
    return json.dumps(app.admin.get_external_groups()), 200, {'Content-Type': 'application/json'}
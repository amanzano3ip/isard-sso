
$(document).on('shown.bs.modal', '#modalAddDesktop', function () {
    modal_add_desktops.columns.adjust().draw();
}); 

$(document).ready(function() {
    var path = "";
    items = [];
    document.getElementById('file-upload').addEventListener('change', readFile, false);
	$('.btn-upload').on('click', function () {
        $('#modalImport').modal({backdrop: 'static', keyboard: false}).modal('show');
        $('#modalImportForm')[0].reset();
	});

    $('.btn-sync').on('click', function () {
        $.ajax({
            type: "PUT",
            url:"/isard-sso-admin/external",
            success: function(data)
            {
                console.log('SUCCESS')
                // $("#modalImport").modal('hide');
                // users_table.ajax.reload();
                // groups_table.ajax.reload();
            },
            error: function(data)
            {
                alert('Something went wrong on our side...')
            }
        });
    });
    
    $("#modalImport #send").on('click', function(e){
        var form = $('#modalImportForm');
        form.parsley().validate();
        if (form.parsley().isValid()){
            formdata = form.serializeObject()
            formdata['data']=JSON.parse(filecontents)
            $.ajax({
                type: "POST",
                url:"/isard-sso-admin/external",
                data: JSON.stringify(formdata),
                success: function(data)
                {
                    $("#modalImport").modal('hide');
                    users_table.ajax.reload();
                    groups_table.ajax.reload();
                },
                error: function(data)
                {
                    alert('Something went wrong on our side...')
                }
            });
        }
    });

	//DataTable Main renderer
	var users_table = $('#users').DataTable({
			"ajax": {
				"url": "/isard-sso-admin/external_users_list",
				"dataSrc": ""
			},
			"language": {
				"loadingRecords": '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                "emptyTable": "<h1>You don't have any user created yet.</h1><br><h2>Create one using the +Add new button on top right of this page.</h2>"
			},           
			"rowId": "id",
			"deferRender": true,
			"columns": [
				{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "width": "10px",
                "defaultContent": '<button class="btn btn-xs btn-info" type="button"  data-placement="top" ><i class="fa fa-plus"></i></button>'
				},
                { "data": "provider", "width": "10px" },
                { "data": "id", "width": "10px" },
				{ "data": "username", "width": "10px"},
				{ "data": "first", "width": "10px"},
				{ "data": "last", "width": "10px"},
                { "data": "email", "width": "10px"},
                { "data": "groups", "width": "10px"},
                { "data": "roles", "width": "10px"},
				],
			 "order": [[3, 'asc']],		 
		"columnDefs": [ ]
    } );
    
	var groups_table = $('#groups').DataTable({
        "ajax": {
            "url": "/isard-sso-admin/external_groups_list",
            "dataSrc": ""
        },
        "language": {
            "loadingRecords": '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "emptyTable": "<h1>You don't have any user created yet.</h1><br><h2>Create one using the +Add new button on top right of this page.</h2>"
        },           
        "rowId": "id",
        "deferRender": true,
        "columns": [
            {
            "className":      'details-control',
            "orderable":      false,
            "data":           null,
            "width": "10px",
            "defaultContent": '<button class="btn btn-xs btn-info" type="button"  data-placement="top" ><i class="fa fa-plus"></i></button>'
            },
            { "data": "id", "width": "10px" },
            { "data": "provider", "width": "10px" },
            { "data": "name", "width": "10px" },
            { "data": "description", "width": "10px"},
            ],
         "order": [[2, 'asc']],		 
    "columnDefs": [ ]
} );
});

function readFile (evt) {
    path = "";
    items = [];
    var files = evt.target.files;
    var file = files[0];
    var reader = new FileReader();
    reader.onload = function(event) {
      filecontents=event.target.result;
        $.each(JSON.parse(filecontents), walker);
        populate_path(items)
    }
    reader.readAsText(file, 'UTF-8')
    
 }

 function toObject(names, values) {
     var result = {};
     for (var i = 0; i < names.length; i++)
          result[names[i]] = values[i];
     return result;
 }

function walker(key, value) {
    var savepath = path;
    path = path ? (path + "/" + key) : key;
    items.push({path:path})

    if (typeof value === "object") {
        // Recurse into children
        if(value.constructor === Array){
            value=value[0]
        }
        if(typeof value == "object"){
            $.each(value, walker);
        }
    }

    path = savepath;
}

function populate_path(){
    $.each(items, function(key, value) {
        $(".populate").append('<option value=' + value['path']+ '>' + value['path'] + '</option>');
    })
}
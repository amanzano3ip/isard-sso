
$(document).on('shown.bs.modal', '#modalAddDesktop', function () {
    modal_add_desktops.columns.adjust().draw();
}); 

$(document).ready(function() {

	$('.btn-new').on('click', function () {
            $("#modalAdd")[0].reset();
            $('#modalAddDesktop').modal({
                backdrop: 'static',
                keyboard: false
            }).modal('show');
            $('#modalAdd').parsley();
	});

    $('.btn-sync_to_moodle').on('click', function () {
        $.ajax({
            type: "POST",
            url:"/isard-sso-admin/users/moodle",
            success: function(data)
            {
                console.log('SUCCESS')
                // $("#modalImport").modal('hide');
                // users_table.ajax.reload();
                // groups_table.ajax.reload();
            },
            error: function(data)
            {
                alert('Something went wrong on our side...')
            }
        });
    });

    $('.btn-sync_to_nextcloud').on('click', function () {
        $.ajax({
            type: "POST",
            url:"/isard-sso-admin/users/nextcloud",
            success: function(data)
            {
                console.log('SUCCESS')
                // $("#modalImport").modal('hide');
                // users_table.ajax.reload();
                // groups_table.ajax.reload();
            },
            error: function(data)
            {
                alert('Something went wrong on our side...')
            }
        });
    });

	//DataTable Main renderer
	var table = $('#users').DataTable({
			"ajax": {
				"url": "/isard-sso-admin/users_list",
				"dataSrc": ""
			},
			"language": {
				"loadingRecords": '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                "emptyTable": "<h1>You don't have any user created yet.</h1><br><h2>Create one using the +Add new button on top right of this page.</h2>"
			},           
			"rowId": "id",
			"deferRender": true,
			"columns": [
				{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "width": "10px",
                "defaultContent": '<button class="btn btn-xs btn-info" type="button"  data-placement="top" ><i class="fa fa-plus"></i></button>'
				},
                { "data": "id", "width": "10px" },
                { "data": "keycloak", "width": "10px" },
                { "data": "keycloak_groups", "width": "10px" },
                { "data": "moodle", "width": "10px" },
                { "data": "moodle_groups", "width": "10px" },
                { "data": "nextcloud", "width": "10px" },
                { "data": "nextcloud_groups", "width": "10px" },
				{ "data": "username", "width": "10px"},
				{ "data": "first", "width": "10px"},
				{ "data": "last", "width": "10px"},
                { "data": "email", "width": "10px"},
				],
			 "order": [[4, 'asc']],		 
		"columnDefs": [ {
							"targets": 2,
							"render": function ( data, type, full, meta ) {
							  if(full.keycloak){
                                    return '<i class="fa fa-check" style="color:lightgreen"></i>'
                                }else{
                                    return '<i class="fa fa-close" style="color:darkred"></i>'
                                };
                            }},
                            {
                            "targets": 4,
                            "render": function ( data, type, full, meta ) {
                                if(full.moodle){
                                    return '<i class="fa fa-check" style="color:lightgreen"></i>'
                                }else{
                                    return '<i class="fa fa-close" style="color:darkred"></i>'
                                };
                            }},
                            {
                            "targets": 6,
                            "render": function ( data, type, full, meta ) {
                                if(full.nextcloud){
                                    return '<i class="fa fa-check" style="color:lightgreen"></i>'
                                }else{
                                    return '<i class="fa fa-close" style="color:darkred"></i>'
                                };
                            }},
							]
	} );
});
from requests import get, post
from admin import app

import logging as log


from .postgres import Postgres

# Module variables to connect to moodle api

class Moodle():
    """https://github.com/mrcinv/moodle_api.py
        https://docs.moodle.org/dev/Web_service_API_functions
        https://docs.moodle.org/311/en/Using_web_services
    """

    def __init__(self,
                key=app.config["MOODLE_WS_TOKEN"],
                url="https://moodle."+app.config["DOMAIN"],
                endpoint="/webservice/rest/server.php",
                verify=app.config["VERIFY"]):
        self.key = key
        self.url = url
        self.endpoint = endpoint
        self.verify=verify

        self.pg=Postgres('isard-apps-postgresql','moodle',app.config['MOODLE_POSTGRES_USER'],app.config['MOODLE_POSTGRES_PASSWORD'])


    def rest_api_parameters(self, in_args, prefix='', out_dict=None):
        """Transform dictionary/array structure to a flat dictionary, with key names
        defining the structure.
        Example usage:
        >>> rest_api_parameters({'courses':[{'id':1,'name': 'course1'}]})
        {'courses[0][id]':1,
         'courses[0][name]':'course1'}
        """
        if out_dict==None:
            out_dict = {}
        if not type(in_args) in (list,dict):
            out_dict[prefix] = in_args
            return out_dict
        if prefix == '':
            prefix = prefix + '{0}'
        else:
            prefix = prefix + '[{0}]'
        if type(in_args)==list:
            for idx, item in enumerate(in_args):
                self.rest_api_parameters(item, prefix.format(idx), out_dict)
        elif type(in_args)==dict:
            for key, item in in_args.items():
                self.rest_api_parameters(item, prefix.format(key), out_dict)
        return out_dict
    
    def call(self, fname, **kwargs):
        """Calls moodle API function with function name fname and keyword arguments.
        Example:
        >>> call_mdl_function('core_course_update_courses',
                               courses = [{'id': 1, 'fullname': 'My favorite course'}])
        """
        parameters = self.rest_api_parameters(kwargs)
        parameters.update({"wstoken": self.key, 'moodlewsrestformat': 'json', "wsfunction": fname})
        response = post(self.url+self.endpoint, parameters, verify=self.verify)
        response = response.json()
        if type(response) == dict and response.get('exception'):
            raise SystemError("Error calling Moodle API\n", response)
        return response

    def create_user(self, email, username, password, first_name='-', last_name='-'):
        data = [{'username': username, 'email':email, 
                 'password': password, 'firstname':first_name, 'lastname':last_name}]
        user = self.call('core_user_create_users', users=data)
        return user

    def get_user_by(self, key, value):
        criteria = [{'key': key, 'value': value}]
        user = self.call('core_user_get_users', criteria=criteria)
        return user

    def get_users_with_groups_and_roles(self):
        q = """select u.id as id, username, firstname as first, lastname as last, email, json_agg(h.name) as groups,  json_agg(r.shortname) as roles
        from mdl_user as u
        LEFT JOIN mdl_cohort_members AS hm on hm.id = u.id
        left join mdl_cohort AS h ON h.id = hm.cohortid 
        left join mdl_role_assignments AS ra ON ra.id = u.id
        left join mdl_role as r on r.id = ra.roleid
        group by u.id , username, first, last, email"""
        (headers,users)=self.pg.select_with_headers(q)
        users_with_lists = [list(l[:-2])+([[]] if l[-2] == [None] else [list(set(l[-2]))]) + ([[]] if l[-1] == [None] else [list(set(l[-1]))]) for l in users]
        list_dict_users = [dict(zip(headers, r)) for r in users_with_lists]
        return list_dict_users

    ## NOT USED. Too slow
    # def get_users_with_groups_and_roles(self):
    #     users=self.get_user_by('email','%%')['users']
    #     for user in users:
    #         user['groups']=[c['name'] for c in self.get_user_cohorts(user['id'])]
    #         user['roles']=[]
    #     return users

    def enroll_user_to_course(self, user_id, course_id, role_id=5):
        # 5 is student
        data = [{'roleid': role_id, 'userid': user_id,  'courseid': course_id}]
        enrolment = self.call('enrol_manual_enrol_users', enrolments=data)
        return enrolment
        
    def get_quiz_attempt(self, quiz_id, user_id):
        attempts = self.call('mod_quiz_get_user_attempts', quizid=quiz_id, userid=user_id)
        return attempts
    
    def get_cohorts(self):
        cohorts = self.call('core_cohort_get_cohorts')
        return cohorts

    def get_cohort_members(self, cohort_id):
        members = self.call('core_cohort_get_cohort_members', cohortids=[cohort_id])[0]['userids']
        return members
    
    def get_user_cohorts(self, user_id):
        user_cohorts=[]
        cohorts=self.get_cohorts()
        for cohort in cohorts:
            if user_id in self.get_cohort_members(cohort['id']): user_cohorts.append(cohort)
        return user_cohorts
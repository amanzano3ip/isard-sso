#!/usr/bin/env python
# coding=utf-8
import time ,os
from admin import app
from datetime import datetime, timedelta

import logging as log
import traceback
import yaml, json
from pprint import pprint

from jinja2 import Environment, FileSystemLoader

from keycloak import KeycloakAdmin

class Keycloak():
    """https://www.keycloak.org/docs-api/13.0/rest-api/index.html
        https://github.com/marcospereirampj/python-keycloak
        https://gist.github.com/kaqfa/99829941121188d7cef8271f93f52f1f
    """
    def __init__(self,
                url="http://isard-sso-keycloak:8080/auth/",
                username=os.environ['KEYCLOAK_USER'],
                password=os.environ['KEYCLOAK_PASSWORD'],
                realm='master',
                verify=True):
        self.url=url
        self.username=username
        self.password=password
        self.realm=realm
        self.verify=verify

    def connect(self):
        self.keycloak_admin = KeycloakAdmin(server_url=self.url,
                                    username=self.username,
                                    password=self.password,
                                    realm_name=self.realm,
                                    verify=self.verify)

# keycloak_admin = KeycloakAdmin(server_url="http://isard-sso-keycloak:8080/auth/",username="admin",password="keycloakkeycloak",realm_name="master",verify=False)

        ######## Example create group and subgroup
        
        # try:
        #     self.add_group('level1')
        # except:
        #     self.delete_group(self.get_group('/level1')['id'])
        # self.add_group('level1')
        # self.add_group('level2',parent=self.get_group('/level1')['id'])
        # pprint(self.get_groups())

        ######## Example roles
        # try:
        #     self.add_role('superman')
        # except:
        #     self.delete_role('superman')
        # self.add_role('superman')
        # pprint(self.get_roles())

    ## USERS

    def get_user_id(self,username):
        self.connect()
        return self.keycloak_admin.get_user_id(username)

    def get_users(self):
        self.connect()
        return self.keycloak_admin.get_users({})

# q = """select u.username,u.email,u.first_name,u.last_name, u.realm_id, ua.value as quota
# ,json_agg(g."name") as group, json_agg(g_parent."name") as group_parent1, json_agg(g_parent2."name") as group_parent2
# ,json_agg(r.name) as role
# from user_entity as u
# left join user_attribute as ua on ua.user_id=u.id and ua.name = 'quota'
# left join user_group_membership as ugm on ugm.user_id = u.id
# left join keycloak_group as g on g.id = ugm.group_id
# left join keycloak_group as g_parent on g.parent_group = g_parent.id
# left join keycloak_group as g_parent2 on g_parent.parent_group = g_parent2.id 
# left join user_role_mapping as rm on rm.user_id = u.id
# left join keycloak_role as r on r.id = rm.role_id
# group by u.username,u.email,u.first_name,u.last_name, u.realm_id, ua.value
# order by u.username"""
# cur.execute(q)
# users = cur.fetchall()
# fields = [a.name for a in cur.description]
# cur.close()
# conn.close()


# users_with_lists = [list(l[:-4])+([[]] if l[-4] == [None] else [list(set(l[-4]))]) +\
#                                  ([[]] if l[-3] == [None] else [list(set(l[-3]))]) +\
#                                  ([[]] if l[-3] == [None] else [list(set(l[-2]))]) +\
#                                  ([[]] if l[-1] == [None] else [list(set(l[-1]))]) for l in users]
                                 
# users_with_lists = [list(l[:-4])+([[]] if l[-4] == [None] else [list(set(l[-4]))]) +\
#                                  ([[]] if l[-3] == [None] else [list(set(l[-3]))]) +\
#                                  ([[]] if l[-3] == [None] else [list(set(l[-2]))]) +\
#                                  ([[]] if l[-1] == [None] else [list(set(l[-1]))]) for l in users_with_lists]

# list_dict_users = [dict(zip(fields, r)) for r in users_with_lists]

    ## Too slow
    def get_users_with_groups_and_roles(self):
        self.connect()
        users=self.keycloak_admin.get_users({})
        for user in users:
            user['groups']=[g['path'] for g in self.keycloak_admin.get_user_groups(user_id=user['id'])]
            user['roles']= [r['name'] for r in self.keycloak_admin.get_realm_roles_of_user(user_id=user['id'])]
        return users

    def add_user(self,username,first,last,email,password,group=False):
        # Returns user id
        log.error('Creating group: '+str(group))
        self.connect()
        username=username.lower()
        try:
            uid=self.keycloak_admin.create_user({"email": email,
                                    "username": username,
                                    "enabled": True,
                                    "firstName": first,
                                    "lastName": last,
                                    "credentials":[{"type":"password",
                                        "value":password,
                                        "temporary":False}]})
        except:
            uid=self.keycloak_admin.get_user_id(username)
            log.error(uid)
        if group:
            try:
                gid=self.keycloak_admin.get_group_by_path(path='/'+group,search_in_subgroups=False)['id']
                log.error('group created with gid: '+str(gid))
            except:
                self.keycloak_admin.create_group({"name":group})
                gid=self.keycloak_admin.get_group_by_path('/'+group)['id']
                log.error(gid)
            self.keycloak_admin.group_user_add(uid,gid)
            

    def add_user_role(self,client_id,user_id,role_id,role_name):
        self.connect()
        return self.keycloak_admin.assign_client_role(client_id="client_id", user_id="user_id", role_id="role_id", role_name="test")

    def delete_user(self,userid):
        self.connect()
        return self.keycloak_admin.delete_user(user_id=userid)

    def get_user_groups(self,userid):
        self.connect()
        return self.keycloak_admin.get_user_groups(user_id=userid)

    ## GROUPS
    def get_groups(self,with_subgroups=True):
        self.connect()
        groups = self.keycloak_admin.get_groups()
        subgroups=[]
        if with_subgroups:
            for group in groups:
                if len(group['subGroups']):
                    for sg in group['subGroups']:
                        subgroups.append(sg)
        return groups+subgroups

    def get_group(self,path,recursive=True):
        self.connect()
        return self.keycloak_admin.get_group_by_path(path=path,search_in_subgroups=recursive)

    def add_group(self,name,parent=None):
        self.connect()
        return self.keycloak_admin.create_group({"name":name}, parent=parent)

    def delete_group(self,group_id):
        self.connect()
        return self.keycloak_admin.delete_group(group_id=group_id)

    ## ROLES
    def get_roles(self):
        self.connect()
        return self.keycloak_admin.get_realm_roles()

    def get_role(self,name):
        self.connect()
        return self.keycloak_admin.get_realm_role(name=name)

    def add_role(self,name):
        self.connect()
        return self.keycloak_admin.create_realm_role({"name":name})

    def delete_role(self,name):
        self.connect()
        return self.keycloak_admin.delete_realm_role(name)


    ## CLIENTS

    def get_client_roles(self,client_id):
        self.connect()
        return self.keycloak_admin.get_client_roles(client_id=client_id)

    # def add_client_role(self,client_id,roleName):
    #     return self.keycloak_admin.create_client_role(client_id=client_id, {'name': roleName, 'clientRole': True})


    ## SYSTEM
    def get_server_info(self):
        self.connect()
        return self.keycloak_admin.get_server_info()
    
    def get_server_clients(self):
        self.connect()
        return self.keycloak_admin.get_clients()

    def get_server_rsa_key(self):
        self.connect()
        rsa_key = [k for k in self.keycloak_admin.get_keys()['keys'] if k['type']=='RSA'][0]
        return {'name':rsa_key['kid'],'certificate':rsa_key['certificate']}

    ## CLIENTS
    def add_moodle_client(self):
        self.connect()
        demo={
            "id" : "a92d5417-92b6-4678-9cb9-51bc0edcee8c",
            "clientId" : "https://moodle."+app.config['DOMAIN']+"/auth/saml2/sp/metadata.php",
            "surrogateAuthRequired" : False,
            "enabled" : True,
            "alwaysDisplayInConsole" : False,
            "clientAuthenticatorType" : "client-secret",
            "redirectUris" : [ "https://moodle."+app.config['DOMAIN']+"/auth/saml2/sp/saml2-acs.php/moodle."+app.config['DOMAIN']+"" ],
            "webOrigins" : [ "https://moodle."+app.config['DOMAIN']+"" ],
            "notBefore" : 0,
            "bearerOnly" : False,
            "consentRequired" : False,
            "standardFlowEnabled" : True,
            "implicitFlowEnabled" : False,
            "directAccessGrantsEnabled" : False,
            "serviceAccountsEnabled" : False,
            "publicClient" : False,
            "frontchannelLogout" : True,
            "protocol" : "saml",
            "attributes" : {
            "saml.force.post.binding" : True,
            "saml.encrypt" : True,
            "saml_assertion_consumer_url_post" : "https://moodle."+app.config['DOMAIN']+"/auth/saml2/sp/saml2-acs.php/moodle."+app.config['DOMAIN']+"",
            "saml.server.signature" : True,
            "saml.server.signature.keyinfo.ext" : False,
            "saml.signing.certificate" : app.config['SP_CRT'],
            "saml_single_logout_service_url_redirect" : "https://moodle."+app.config['DOMAIN']+"/auth/saml2/sp/saml2-logout.php/moodle."+app.config['DOMAIN']+"",
            "saml.signature.algorithm" : "RSA_SHA256",
            "saml_force_name_id_format" : False,
            "saml.client.signature" : True,
            "saml.encryption.certificate" : app.config['SP_PEM'],
            "saml.authnstatement" : True,
            "saml_name_id_format" : "username",
            "saml_signature_canonicalization_method" : "http://www.w3.org/2001/10/xml-exc-c14n#"
            },
            "authenticationFlowBindingOverrides" : { },
            "fullScopeAllowed" : True,
            "nodeReRegistrationTimeout" : -1,
            "protocolMappers" : [ {
            "id" : "9296daa3-4fc4-4b80-b007-5070f546ae13",
            "name" : "X500 surname",
            "protocol" : "saml",
            "protocolMapper" : "saml-user-property-mapper",
            "consentRequired" : False,
            "config" : {
                "attribute.nameformat" : "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",
                "user.attribute" : "lastName",
                "friendly.name" : "surname",
                "attribute.name" : "urn:oid:2.5.4.4"
            }
            }, {
            "id" : "ccecf6e4-d20a-4211-b67c-40200a6b2c5d",
            "name" : "username",
            "protocol" : "saml",
            "protocolMapper" : "saml-user-property-mapper",
            "consentRequired" : False,
            "config" : {
                "attribute.nameformat" : "Basic",
                "user.attribute" : "username",
                "friendly.name" : "username",
                "attribute.name" : "username"
            }
            }, {
            "id" : "53858403-eba2-4f6d-81d0-cced700b5719",
            "name" : "X500 givenName",
            "protocol" : "saml",
            "protocolMapper" : "saml-user-property-mapper",
            "consentRequired" : False,
            "config" : {
                "attribute.nameformat" : "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",
                "user.attribute" : "firstName",
                "friendly.name" : "givenName",
                "attribute.name" : "urn:oid:2.5.4.42"
            }
            }, {
            "id" : "20034db5-1d0e-4e66-b815-fb0440c6d1e2",
            "name" : "X500 email",
            "protocol" : "saml",
            "protocolMapper" : "saml-user-property-mapper",
            "consentRequired" : False,
            "config" : {
                "attribute.nameformat" : "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",
                "user.attribute" : "email",
                "friendly.name" : "email",
                "attribute.name" : "urn:oid:1.2.840.113549.1.9.1"
            }
            } ],
            "defaultClientScopes" : [ "web-origins", "role_list", "roles", "profile", "email" ],
            "optionalClientScopes" : [ "address", "phone", "offline_access", "microprofile-jwt" ],
            "access" : {
            "view" : True,
            "configure" : True,
            "manage" : True
            }
        }
        return self.keycloak_admin.create_client(demo)
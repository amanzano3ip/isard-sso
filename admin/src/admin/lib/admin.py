from admin import app
from .keycloak import Keycloak
from .moodle import Moodle
from .nextcloud import Nextcloud

import logging as log
from pprint import pprint

from .nextcloud_exc import *

class Admin():
    def __init__(self):
        self.keycloak=Keycloak(verify=app.config['VERIFY'])
        self.moodle=Moodle(verify=app.config['VERIFY'])
        self.nextcloud=Nextcloud(verify=app.config['VERIFY'])

        # self.internal={'users':self.get_mix_users(),
        #                 'groups':self.get_mix_groups(),
        #                 'roles':[]}
        self.external={'users':[],
                        'groups':[],
                        'roles':[]}
        #pprint(self.get_moodle_groups())
        # pprint(self.get_moodle_users())
        # pprint(self.get_keycloak_users())
        # pprint(self.get_nextcloud_users())

    def get_moodle_users(self):
        return self.moodle.get_users_with_groups_and_roles()

    ## TOO SLOW. Not used.
    # def get_moodle_users(self):
    #     log.warning('Loading moodle users... can take a long time...')
    #     users = self.moodle.get_users_with_groups_and_roles()
    #     #self.moodle.get_user_by('email','%%')['users']
    #     return [{"id":u['id'],
    #             "username":u['username'],
    #             "first": u['firstname'], 
    #             "last": u['lastname'], 
    #             "email": u['email'],
    #             "groups": u['groups'],
    #             "roles": u['roles']} 
    #             for u in users]

    def get_keycloak_users(self):
        log.warning('Loading keycloak users... can take a long time...')
        users = self.keycloak.get_users_with_groups_and_roles()
        return [{"id":u['id'],
                "username":u['username'],
                "first": u.get('firstName',None), 
                "last": u.get('lastName',None), 
                "email": u.get('email',''),
                "groups": u['groups'],
                "roles": u['roles']} 
                for u in users]

    def get_nextcloud_users(self):
        log.warning('Loading nextcloud users... can take a long time...')
        users = self.nextcloud.get_users_list()
        users_list=[]
        for user in users:
            u=self.nextcloud.get_user(user)
            users_list.append({"id":u['id'],
                            "username":u['id'],
                            "first": u['displayname'], 
                            "last": None, 
                            "email": u['email'],
                            "groups": u['groups'],
                            "roles": []})
        return users_list

    # def get_ram_users(self):
    #     return self.internal['users']

    def get_mix_users(self):
        kusers=self.get_keycloak_users()
        musers=self.get_moodle_users()
        # nusers=self.get_nextcloud_users()
        nusers=[]

        kusers_usernames=[u['username'] for u in kusers]
        musers_usernames=[u['username'] for u in musers]
        nusers_usernames=[u['username'] for u in nusers]

        all_users_usernames=set(kusers_usernames+musers_usernames+nusers_usernames)

        users=[]
        for username in all_users_usernames:
            theuser={}
            keycloak_exists=[u for u in kusers if u['username'] == username]
            if len(keycloak_exists):
                theuser=keycloak_exists[0]
                theuser['keycloak']=True
                theuser['keycloak_groups']=keycloak_exists[0]['groups']
            else:
                theuser['id']=False
                theuser['keycloak']=False
                theuser['keycloak_groups']=[]

            moodle_exists=[u for u in musers if u['username'] == username]
            if len(moodle_exists):
                theuser={**moodle_exists[0], **theuser}
                theuser['moodle']=True
                theuser['moodle_groups']=moodle_exists[0]['groups']
            else:
                theuser['moodle']=False
                theuser['moodle_groups']=[]

            nextcloud_exists=[u for u in nusers if u['username'] == username]
            if len(nextcloud_exists):
                theuser={**nextcloud_exists[0], **theuser}
                theuser['nextcloud']=True
                theuser['nextcloud_groups']=nextcloud_exists[0]['groups']
            else:
                theuser['nextcloud']=False
                theuser['nextcloud_groups']=[]
            del theuser['groups']
            users.append(theuser)
        
        return users

    def get_roles(self):
        return self.keycloak.get_roles()

    def get_keycloak_groups(self):
        log.warning('Loading keycloak groups... can take a long time...')
        return self.keycloak.get_groups()

    def get_moodle_groups(self):
        log.warning('Loading moodle groups... can take a long time...')
        return self.moodle.get_cohorts()

    def get_nextcloud_groups(self):
        log.warning('Loading nextcloud groups... can take a long time...')
        return self.nextcloud.get_groups_list()

    # def get_ram_groups(self):
    #     return self.internal['groups']

    def get_mix_groups(self):
        kgroups=self.get_keycloak_groups() 
        mgroups=self.get_moodle_groups()
        ngroups=self.get_nextcloud_groups()

        kgroups=[] if kgroups is None else kgroups
        mgroups=[] if mgroups is None else mgroups
        ngroups=[] if ngroups is None else ngroups

        kgroups_names=[g['name'] for g in kgroups]
        mgroups_names=[g['name'] for g in mgroups]
        ngroups_names=ngroups

        all_groups_names=set(kgroups_names+mgroups_names+ngroups_names)

        groups=[]
        for name in all_groups_names:
            thegroup={}
            keycloak_exists=[g for g in kgroups if g['name'] == name]
            if len(keycloak_exists):
                thegroup=keycloak_exists[0]
                thegroup['keycloak']=True
            else:
                thegroup['id']=False
                thegroup['keycloak']=False

            moodle_exists=[g for g in mgroups if g['name'] == name]
            if len(moodle_exists):
                thegroup['path']=''
                thegroup={**moodle_exists[0], **thegroup}
                thegroup['moodle']=True
            else:
                thegroup['moodle']=False

            nextcloud_exists=[g for g in ngroups if g == name] 
            if len(nextcloud_exists):
                nextcloud={"id":nextcloud_exists[0],
                            "name":nextcloud_exists[0],
                            "path":''}
                thegroup={**nextcloud, **thegroup}
                thegroup['nextcloud']=True
            else:
                thegroup['nextcloud']=False

            groups.append(thegroup)
        
        return groups

    def get_external_users(self):
        return self.external['users']

    def get_external_groups(self):
        return self.external['groups']

    def get_external_roles(self):
        return self.external['roles']

    def upload_json(self,data):
        groups=[]
        for g in data['data']['groups']:
            # for m in data['data']['d_members']:

            groups.append({'provider':data['provider'],
                            "id": g['id'],
                            "name": g['name'],
                            "description": g['description']})
        self.external['groups']=groups


        users=[]
        for u in data['data']['users']:
            users.append({'provider':data['provider'],
                'id':u['id'],
                'email': u['primaryEmail'],
                'first': u['name']['givenName'],
                'last': u['name']['familyName'],
                'username': u['primaryEmail'].split('@')[0],
                'groups':[u['orgUnitPath']],
                'roles':[]})
        self.external['users']=users

        return True

    def sync_external(self):
        for u in self.external['users']:
            log.error('Creating user: '+u['username'])
            self.keycloak.add_user(u['username'],u['first'],u['last'],u['email'],'1Provaprovaprova',group=u['groups'][0])

    def sync_to_moodle(self):
        users=self.get_mix_users()
        for u in users:
            if not u['moodle']:
                log.error('Creating moodle user: '+u['username'])
                self.moodle.create_user(u['email'],u['username'],'-1Provaprovaprova',u['first'],u['last'])

    def sync_to_nextcloud(self):
        users=self.get_mix_users()
        for u in users:
            if not u['nextcloud']:
                log.error('Creating nextcloud user: '+u['username'])
                group=u['keycloak_groups'][0] if len(u['keycloak_groups']) else False
                try:
                    self.nextcloud.add_user(u['username'],'-1Provaprovaprova',1000,group,u['email'],u['first']+' '+u['last'])
                except ProviderItemExists:
                    log.error('User '+u['username']+' already exists. Skipping...')
                    continue
                except:
                    log.error(traceback.format_exc())
        # <div>Las contraseñas deben tener al menos 1 dígito(s).</div><div>Las contraseñas deben tener al menos 1 mayúscula(s).</div><div>Las contraseñas deben tener al menos 1 caracter(es) no alfanumérico(s) como *,-, 

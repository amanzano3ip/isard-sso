#!flask/bin/python
# coding=utf-8
from gevent import monkey
monkey.patch_all()


from admin import app

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9000, debug=False) #, logger=logger, engineio_logger=engineio_logger)
